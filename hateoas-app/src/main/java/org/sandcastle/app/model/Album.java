package org.sandcastle.app.model;

/**
 * Model class for test
 *
 * @author saumil
 */
public class Album {
    private final String id;
    private final String title;
    private final Artist artist;
    private int stockLevel;

    public Album(String id, String title, Artist artist, int stockLevel) {
        this.id = id;
        this.title = title;
        this.artist = artist;
        this.stockLevel = stockLevel;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Artist getArtist() {
        return artist;
    }

    public int getStockLevel() {
        return stockLevel;
    }

    public void setStockLevel(int stockLevel) {
        this.stockLevel = stockLevel;
    }
}
