package org.sandcastle.app.model;

/**
 * Created on 2016/12/04.
 *
 * @author saumil
 */
public class Artist {
    private final String id;
    private final String name;

    public Artist(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
