package org.sandcastle.app.controllers;

import org.sandcastle.app.model.Artist;
import org.sandcastle.app.service.MusicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


/**
 * Created on 2016/12/04.
 *
 * @author saumil
 */
@Controller
public class ArtistController {
    @Autowired
    private MusicService musicService;

    @RequestMapping(value = "/artist/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Resource<Artist> getArtist(@PathVariable(value = "id") String id){
        Artist a = musicService.getArtist(id);
        Resource<Artist> resource = new Resource<>(a);
        resource.add(linkTo(methodOn(ArtistController.class).getArtist(id)).withSelfRel());
        return resource;
    }

    @RequestMapping(value = "/artists", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<Resource<Artist>> getAllArtists(){
        Collection<Artist> albums = musicService.getAllArtists();
        return albums.stream().map(this::getArtistResource).collect(Collectors.toList());
    }

    private Resource<Artist> getArtistResource(Artist artist) {
        Resource<Artist> resource = new Resource<>(artist);
        resource.add(linkTo(methodOn(ArtistController.class).getArtist(artist.getId())).withSelfRel());
        return resource;
    }
}
