package org.sandcastle.app.service;

import org.sandcastle.app.model.Album;
import org.sandcastle.app.model.Artist;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created on 2016/12/04.
 *
 * @author saumil
 */
@Service
public class MusicService {
    private static Map<String, Album> ALBUMS;
    private static Map<String, Artist> ARTISTS;

    static{
        ARTISTS = new HashMap<>();
        Artist artist1 = new Artist("opeth", "Opeth");
        Artist artist2 = new Artist("cfrost", "Celtic Frost");
        ARTISTS.put(artist1.getId(), artist1);
        ARTISTS.put(artist2.getId(), artist2);

        ALBUMS = new HashMap<>();
        Album album1 = new Album("1", "Heritage", ARTISTS.get("opeth"), 2);
        Album album2 = new Album("2", "Deliverance", ARTISTS.get("opeth"), 3);
        Album album3 = new Album("3", "Pale Communion", ARTISTS.get("opeth"), 0);
        Album album4 = new Album("4", "Monotheise", ARTISTS.get("cfrost"), 1);
        ALBUMS.put(album1.getId(), album1);
        ALBUMS.put(album2.getId(), album2);
        ALBUMS.put(album3.getId(), album3);
        ALBUMS.put(album4.getId(), album4);
    }


    public Collection<Album> getAllAlbums(){
        return ALBUMS.values();
    }

    public Album getAlbum(final String id){
        return ALBUMS.get(id);
    }

    public Artist getArtist(final String id){
        return ARTISTS.get(id);
    }

    public Collection<Artist> getAllArtists() {
        return ARTISTS.values();
    }
}
